package com.test.module;

import com.test.service.Add;
import org.apache.tapestry5.ioc.ServiceBinder;

public class ApplicationModule {
    public static void bind(ServiceBinder binder){
        binder.bind(Add.class);
    }
}
