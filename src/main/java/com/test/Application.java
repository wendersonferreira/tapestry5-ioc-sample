package com.test;

import com.test.service.Add;
import com.test.module.ApplicationModule;
import org.apache.tapestry5.ioc.Registry;
import org.apache.tapestry5.ioc.RegistryBuilder;

public class Application {
    public static void main(String[] args) {
        RegistryBuilder builder = new RegistryBuilder();
        builder.add(ApplicationModule.class);

        Registry registry = builder.build();
        registry.performRegistryStartup();

        Add add = registry.getService(Add.class);
        final int result = add.add(2, 1);
        System.out.println("The service result is: "+result);
    }
}
